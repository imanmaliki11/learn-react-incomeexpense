import React, { useContext, useState } from 'react'
import { GlobalContext } from '../context/GlobalState';

export const AddTransaction = () => {

    const [transName, setTransName] = useState("");
    const [amount, setAmount] = useState();

    const { addTransaction } = useContext(GlobalContext);

    const sumbitNewTransaction = e => {
        e.preventDefault();
        const newTrasnaction = {
            id: Math.floor(Math.random() * 1000000),
            text: transName,
            amount: +amount
        }
        addTransaction(newTrasnaction);
        setAmount("");
        setTransName("");
    }
    return (
        <>
            <h3>Add new transaction</h3>
            <form onSubmit={sumbitNewTransaction}>
                <div className="form-control">
                    <label htmlFor="text">Tag</label>
                    <input type="text" value={transName} onChange={ (e) => setTransName(e.target.value)} placeholder="Enter text..." />
                </div>
                <div className="form-control">
                    <label htmlFor="amount"
                    >Amount <br />
                        (negative - expense, positive - income)</label
                    >
                    <input type="number" value={ amount } onChange={(e) => setAmount(e.target.value)} placeholder="Enter amount..." />
                </div>
                <button className="btn">Add transaction</button>
            </form>
        </>
    )
}
