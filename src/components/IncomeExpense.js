import React, { useContext } from 'react'
import { GlobalContext } from '../context/GlobalState'
import { formatMoney } from '../Helper/Helpers'

export const IncomeExpense = () => {
    const { transactions } = useContext(GlobalContext);
    const amount = transactions.map(trans => trans.amount);
    const income = amount.filter(item => item > 0).reduce((add, i) => (add += i), 0).toFixed(0);
    const expense = amount.filter(item => item < 0).reduce((add, i) => (add += (i * (-1))), 0).toFixed(0);
  return (
      <div className="inc-exp-container">
          <div>
              <h4>Income</h4>
              <p className="money plus">Rp. {formatMoney(income)}</p>
          </div>
          <div>
              <h4>Expense</h4>
              <p className="money minus">Rp. {formatMoney(expense)}</p>
          </div>
      </div>
  )
}
