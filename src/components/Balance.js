import React, { useContext } from 'react'
import { GlobalContext } from '../context/GlobalState'
import { formatMoney } from '../Helper/Helpers';

export const Balance = () => {
  const { transactions } = useContext(GlobalContext)
  const amount = transactions.map(trans => trans.amount);
  const total = amount.reduce((acc, item) => (acc += item), 0).toFixed(0);
  return (
    <>
      <h4>Your Balance</h4>
      <h1>Rp. {formatMoney(total)}</h1>
    </>
  )
}
