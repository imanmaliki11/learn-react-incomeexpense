import React, {useContext} from 'react';
import { GlobalContext } from '../context/GlobalState';
import { Transaction } from './Transaction';

export const HistoryTransactions = () => {
  const { transactions } = useContext(GlobalContext);
  return (
      <>
        <h3>History</h3>
        <ul className="list">
        {transactions.map(trans => (<Transaction trans={trans} key={trans.id} />))}
        </ul>
        {transactions.length === 0 ? (<div className='alert'>Empty</div>) : ""}
      </>
  )
}
