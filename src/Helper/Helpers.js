export function formatMoney (money) {
    if(!money) return;
    money = money.toString();
    return money.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}