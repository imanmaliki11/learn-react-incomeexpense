import React, { createContext, useReducer, useEffect } from 'react';
import AppReducer from './AppReducer'

const initialState = {
    transaction: localStorage.getItem('items') ? JSON.parse(localStorage.getItem('items')) : []
}

export const GlobalContext = createContext(initialState);

export const GlobalProvider = ({ children }) => {

    const [state, dispatch] = useReducer(AppReducer, initialState);

    const deleteTransaction = (id) => {
        dispatch({
            type: "DELETE_TRANSACTION",
            payload: id
        });
    }

    const addTransaction = (transaction) => {
        dispatch({
            type: "ADD_TRANSACTION",
            payload: transaction
        });
    }

    useEffect(() => {
        localStorage.setItem('items', JSON.stringify(state.transaction));
    }, [state.transaction]);

    return (<GlobalContext.Provider value={{
        transactions: state.transaction,
        deleteTransaction,
        addTransaction
    }}>
        {children}
    </GlobalContext.Provider>);
}